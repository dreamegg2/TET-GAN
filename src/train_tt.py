from __future__ import print_function
from options import TrainOptions
import torch
from tqdm import tqdm
from models import TETGAN
from utils import *
import os
from torch.utils.tensorboard import SummaryWriter

# os.environ["CUDA_VISIBLE_DEVICES"] = "0"

def Validate_net(model, opts, idx):
    # Validation
    model.eval()

    print('---Vaild ---')
    paths = os.listdir(opts.valid_path + "imagesA")
    # directory
    result_dir = os.path.join(".", opts.test_label, "Vaild_{0}".format(idx))
    print(result_dir)
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)

    for path in paths:
        style_path = opts.valid_path + "imagesA/" + path
        content_path = opts.valid_path + "imagesB_mask/" + path
        gt_path = opts.valid_path + "imagesB/" + path

        style = to_var(load_image(style_path))
        content = to_var(load_image(content_path))
        # GT = to_data(load_image(gt_path))
        # save_image(GT, os.path.join(result_dir, path.split('.')[0]+"_gt.png"))

        result = model(content, style)
        result = to_data(result)

        result_filename = os.path.join(result_dir, path)
        save_image(result[0], result_filename)

    model.train()


def main():
    # parse options
    parser = TrainOptions()
    opts = parser.parse()

    # data loader
    print('--- load parameter ---')
    outer_iter = opts.outer_iter
    fade_iter = max(1.0, float(outer_iter / 2))
    epochs = opts.epoch
    batchsize = opts.batchsize
    datasize = opts.datasize
    datarange = opts.datarange
    augementratio = opts.augementratio
    centercropratio = opts.centercropratio

    writer = SummaryWriter()
    global_step = 0
    logging_freq = 10
    save_freq = 10

    # model
    print('--- create model ---')
    tetGAN = TETGAN(gpu=(opts.gpu != 0))
    if opts.resume_path is not None:
        print('--- load and resume model from ', opts.resume_path, '---')
        tetGAN.load_state_dict(torch.load(opts.resume_path))
        opts.progressive = 1
    if opts.gpu != 0:
        tetGAN.cuda()
    tetGAN.init_networks(weights_init)
    tetGAN.train()

    print('--- training ---')
    stylenames = os.listdir(opts.train_path)
    print('List of %d styles:' % (len(stylenames)), *stylenames, sep=' ')
    idx = 0

    if opts.progressive == 1:  # Loop level
        for level in range(1, 4):
            level_label = 'Lavel_{0}'.format(level)
            print("====", level_label, " Training === with ", batchsize * (2 ** (4 - level)), "====")

            if level == 3:
                outer_iter = outer_iter * 10

            for i in range(outer_iter):
                jitter = min(1.0, i / fade_iter)
                fnames = load_trainset_batchfnames_custom(opts.train_path, batchsize * (2 ** (4 - level)), datarange,
                                                          datasize * 2)
                print("length of fnames ", len(fnames))
                w = 0  # init

                for epoch in range(epochs):
                    idx = idx + 1
                    for fname in tqdm(fnames):
                        # print("fname ", fname)
                        x, y_real, y = prepare_batch(fname, True, level, jitter,
                                                     centercropratio, augementratio, opts.gpu)
                        if level == 1:
                            losses = tetGAN.one_pass(x[0], None, y[0], None, y_real[0], None, level, None)
                        else:
                            losses = tetGAN.one_pass(x[0], x[1], y[0], y[1], y_real[0], y_real[1], level, w)

                        if global_step % save_freq == 0:
                            writer.add_scalar(level_label+"/Lrec", losses[0], global_step)
                            writer.add_scalar(level_label+"/Ldadv", losses[1], global_step)
                            writer.add_scalar(level_label+"/Ldesty", losses[2], global_step)
                            writer.add_scalar(level_label+"/Lsadv", losses[3], global_step)
                            writer.add_scalar(level_label+"/Lsadv", losses[4], global_step)

                            writer.add_image(level_label+"/X", x[0][0], global_step)
                            writer.add_image(level_label+"/Y_real", y_real[0][0], global_step)
                            writer.add_image(level_label+"/Y", y[0][0], global_step)
                            writer.add_image(level_label+"/X_AE", losses[5][0], global_step)
                            writer.add_image(level_label+"/X_DeSty", losses[6][0], global_step)
                            writer.add_image(level_label+"/Y_Sy", losses[7][0], global_step)
                        global_step += 1

                    print(level_label, ', Iter[%d/%d], Epoch [%d/%d]' % (i + 1, outer_iter, epoch + 1, epochs))
                    print('Lrec: %.3f, Ldadv: %.3f, Ldesty: %.3f, Lsadv: %.3f, Lsty: %.3f'
                          % (losses[0], losses[1], losses[2], losses[3], losses[4]))

                    Validate_net(tetGAN, opts, global_step)

                w = max(0.0, 1 - i / fade_iter)

                save_path = "./" + opts.test_label + "/save/"
                if not os.path.exists(save_path):
                    os.mkdir(save_path)
                torch.save(tetGAN.state_dict(), save_path + level_label + "_" + str(i) + ".ckpt")
    else:
        print('directly train on level3 256*256')
        level_label = "directly_L3"
        for i in range(outer_iter):
            # fnames = load_trainset_batchfnames(opts.train_path, batchsize, datarange, datasize)
            fnames = load_trainset_batchfnames_custom(opts.train_path, batchsize * 2, datarange, datasize * 2)
            for epoch in range(epochs):
                idx = idx + 1
                for fname in tqdm(fnames):
                    x, y_real, y = prepare_batch(fname, True, 3, 1,
                                                 centercropratio, augementratio, opts.gpu)
                    losses = tetGAN.one_pass(x[0], None, y[0], None, y_real[0], None, 3, 0)
                    if global_step % save_freq == 0:
                        writer.add_scalar(level_label + "/Lrec", losses[0], global_step)
                        writer.add_scalar(level_label + "/Ldadv", losses[1], global_step)
                        writer.add_scalar(level_label + "/Ldesty", losses[2], global_step)
                        writer.add_scalar(level_label + "/Lsadv", losses[3], global_step)
                        writer.add_scalar(level_label + "/Lsadv", losses[4], global_step)

                        writer.add_image(level_label + "/X", x[0][0], global_step)
                        writer.add_image(level_label + "/Y_real", y_real[0][0], global_step)
                        writer.add_image(level_label + "/Y", y[0][0], global_step)
                        writer.add_image(level_label + "/X_AE", losses[5][0], global_step)
                        writer.add_image(level_label + "/X_DeSty", losses[6][0], global_step)
                        writer.add_image(level_label + "/Y_Sy", losses[7][0], global_step)
                    global_step += 1

                print('Iter[%d/%d], Epoch [%d/%d]' % (i + 1, outer_iter, epoch + 1, epochs))
                print('Lrec: %.3f, Ldadv: %.3f, Ldesty: %.3f, Lsadv: %.3f, Lsty: %.3f'
                      % (losses[0], losses[1], losses[2], losses[3], losses[4]))

                Validate_net(tetGAN, opts, idx)

                save_path = "./" + opts.test_label + "/save/"
                if not os.path.exists(save_path):
                    os.mkdir(save_path)
                torch.save(tetGAN.state_dict(), save_path + "ckp_" + str(idx) + ".ckpt")

    print('--- save ---')
    torch.save(tetGAN.state_dict(), "./" + opts.test_label + "/save/ckp.ckpt")


if __name__ == '__main__':
    main()
