from __future__ import print_function
from options import TrainOptions
import torch
from tqdm import tqdm
from models import TETGAN
from utils import *
import os
#os.environ["CUDA_VISIBLE_DEVICES"] = "0"

from torch.utils.tensorboard import SummaryWriter


def Validate_net(model,opts,idx):
    # Validation
    model.eval()

    print('---Vaild ---')
    paths = os.listdir(opts.valid_path + "imagesA")
    # directory
    result_dir = os.path.join(".", opts.test_label, "Vaild_{0}".format(idx))
    print(result_dir)
    if not os.path.exists(result_dir):
        os.mkdir(result_dir)

    for path in paths:
        style_path = opts.valid_path + "imagesA/" + path
        content_path = opts.valid_path + "imagesB_mask/" + path
        gt_path = opts.valid_path + "imagesB/" + path

        style = to_var(load_image(style_path))
        content = to_var(load_image(content_path))
        # GT = to_data(load_image(gt_path))
        # save_image(GT, os.path.join(result_dir, path.split('.')[0]+"_gt.png"))

        result = model(content, style)
        result = to_data(result)

        result_filename = os.path.join(result_dir, path)
        save_image(result[0], result_filename)

    model.train()

def main():
    # parse options
    parser = TrainOptions()
    opts = parser.parse()

    # data loader
    print('--- load parameter ---')
    outer_iter = opts.outer_iter
    fade_iter = max(1.0, float(outer_iter / 2))
    epochs = opts.epoch
    batchsize = opts.batchsize
    datasize = opts.datasize
    datarange = opts.datarange
    augementratio = opts.augementratio
    centercropratio = opts.centercropratio      
    
    # model
    print('--- create model ---')
    tetGAN = TETGAN(gpu = (opts.gpu!=0))
    if opts.gpu != 0:
        tetGAN.cuda()
    tetGAN.init_networks(weights_init)
    tetGAN.train()

    print('--- training ---')
    stylenames = os.listdir(opts.train_path)
    print('List of %d styles:'%(len(stylenames)), *stylenames, sep=' ')

    writer = SummaryWriter()
    global_step = 0
    logging_freq = 10
    save_freq = 10

    if opts.progressive == 1:
        # proressive training. From level1 64*64, to level2 128*128, to level3 256*256
        # level 1
        for i in range(outer_iter):
            jitter = min(1.0, i / fade_iter)
            #fnames = load_trainset_batchfnames(opts.train_path, batchsize*4, datarange, datasize*2)
            fnames = load_trainset_batchfnames_custom(opts.train_path, batchsize * 8, datarange, datasize * 2)
            print("length of fnames ", len(fnames))
            for epoch in range(epochs):
                for fname in tqdm(fnames):
                    #print("fname ", fname)
                    '''
                    x, y_real, y = prepare_batch(fname, False, 1, jitter,
                                                 centercropratio, augementratio, opts.gpu)
                    '''
                    x, y_real, y = prepare_batch(fname, True, 1, jitter,
                                                 centercropratio, augementratio, opts.gpu)
                    losses = tetGAN.one_pass(x[0], None, y[0], None, y_real[0], None, 1, None)
                    # return [Lrec, Ldadv, Ldesty, Lsadv, Lsty, x_ae, x_desty, y_st]

                    if global_step % save_freq == 0:
                        writer.add_scalar("Level1/Lrec", losses[0], global_step)
                        writer.add_scalar("Level1/Ldadv", losses[1], global_step)
                        writer.add_scalar("Level1/Ldesty", losses[2], global_step)
                        writer.add_scalar("Level1/Lsadv", losses[3], global_step)
                        writer.add_scalar("Level1/Lsadv", losses[4], global_step)

                        writer.add_image("Level1/X", x[0][0], global_step)
                        writer.add_image("Level1/Y_real", y_real[0][0], global_step)
                        writer.add_image("Level1/Y", y[0][0], global_step)
                        writer.add_image("Level1/X_AE", losses[5][0], global_step)
                        writer.add_image("Level1/X_DeSty", losses[6][0], global_step)
                        writer.add_image("Level1/Y_Sy", losses[7][0], global_step)
                    global_step += 1

                Validate_net(tetGAN, opts, global_step)
                print('Level1, Iter[%d/%d], Epoch [%d/%d]' %(i+1, outer_iter, epoch+1, epochs))
                print('Lrec: %.3f, Ldadv: %.3f, Ldesty: %.3f, Lsadv: %.3f, Lsty: %.3f'
                         %(losses[0], losses[1], losses[2], losses[3], losses[4]))
        # level 2
        for i in range(outer_iter):
            w = max(0.0, 1 - i / fade_iter)
            #fnames = load_trainset_batchfnames(opts.train_path, batchsize*2, datarange, datasize*2)
            fnames = load_trainset_batchfnames_custom(opts.train_path, batchsize * 4, datarange, datasize * 2)
            for epoch in range(epochs):
                for fname in tqdm(fnames):
                    #print("fname ", fname)
                    '''
                    x, y_real, y = prepare_batch(fname, False, 2, 1,
                                                 centercropratio, augementratio, opts.gpu)
                    '''
                    x, y_real, y = prepare_batch(fname, True, 2, 1,
                                                 centercropratio, augementratio, opts.gpu)
                    losses = tetGAN.one_pass(x[0], x[1], y[0], y[1], y_real[0], y_real[1], 2, w)
                    if global_step % save_freq == 0:
                        writer.add_scalar("Level2/Lrec", losses[0], global_step)
                        writer.add_scalar("Level2/Ldadv", losses[1], global_step)
                        writer.add_scalar("Level2/Ldesty", losses[2], global_step)
                        writer.add_scalar("Level2/Lsadv", losses[3], global_step)
                        writer.add_scalar("Level2/Lsadv", losses[4], global_step)

                        writer.add_image("Level2/X", x[0][0], global_step)
                        writer.add_image("Level2/Y_real", y_real[0][0], global_step)
                        writer.add_image("Level2/Y", y[0][0], global_step)
                        writer.add_image("Level2/X_AE", losses[5][0], global_step)
                        writer.add_image("Level2/X_DeSty", losses[6][0], global_step)
                        writer.add_image("Level2/Y_Sy", losses[7][0], global_step)
                    global_step += 1
                print('Level2, Iter[%d/%d], Epoch [%d/%d]' %(i+1, outer_iter, epoch+1, epochs))
                print('Lrec: %.3f, Ldadv: %.3f, Ldesty: %.3f, Lsadv: %.3f, Lsty: %.3f'
                         %(losses[0], losses[1], losses[2], losses[3], losses[4]))
        # level 3
        for i in range(outer_iter):
            w = max(0.0, 1 - i / fade_iter)
            #fnames = load_trainset_batchfnames(opts.train_path, batchsize, datarange, datasize)
            fnames = load_trainset_batchfnames_custom(opts.train_path, batchsize * 2, datarange, datasize * 2)
            for epoch in range(epochs):
                for fname in tqdm(fnames):
                    #print("fname ", fname)
                    '''
                    x, y_real, y = prepare_batch(fname, False, 3, 1,
                                                 centercropratio, augementratio, opts.gpu)
                    '''
                    x, y_real, y = prepare_batch(fname, True, 3, 1,
                                                 centercropratio, augementratio, opts.gpu)
                    losses = tetGAN.one_pass(x[0], x[1], y[0], y[1], y_real[0], y_real[1], 3, w)
                    if global_step % save_freq == 0:
                        writer.add_scalar("Level3/Lrec", losses[0], global_step)
                        writer.add_scalar("Level3/Ldadv", losses[1], global_step)
                        writer.add_scalar("Level3/Ldesty", losses[2], global_step)
                        writer.add_scalar("Level3/Lsadv", losses[3], global_step)
                        writer.add_scalar("Level3/Lsadv", losses[4], global_step)

                        writer.add_image("Level3/X", x[0][0], global_step)
                        writer.add_image("Level3/Y_real", y_real[0][0], global_step)
                        writer.add_image("Level3/Y", y[0][0], global_step)
                        writer.add_image("Level3/X_AE", losses[5][0], global_step)
                        writer.add_image("Level3/X_DeSty", losses[6][0], global_step)
                        writer.add_image("Level3/Y_Sy", losses[7][0], global_step)
                    global_step += 1
                print('Level3, Iter[%d/%d], Epoch [%d/%d]' %(i+1, outer_iter, epoch+1, epochs))
                print('Lrec: %.3f, Ldadv: %.3f, Ldesty: %.3f, Lsadv: %.3f, Lsty: %.3f'
                         %(losses[0], losses[1], losses[2], losses[3], losses[4])) 
    else:
        # directly train on level3 256*256
        for i in range(outer_iter):
            # fnames = load_trainset_batchfnames(opts.train_path, batchsize, datarange, datasize)
            fnames = load_trainset_batchfnames_custom(opts.train_path, batchsize * 2, datarange, datasize * 2)
            for epoch in range(epochs):
                for fname in fnames:
                    x, y_real, y = prepare_batch(fname, True, 3, 1,
                                                 centercropratio, augementratio, opts.gpu)
                    losses = tetGAN.one_pass(x[0], None, y[0], None, y_real[0], None, 3, 0)
                    if global_step % save_freq == 0:
                        writer.add_scalar("Level/Lrec", losses[0], global_step)
                        writer.add_scalar("Level/Ldadv", losses[1], global_step)
                        writer.add_scalar("Level/Ldesty", losses[2], global_step)
                        writer.add_scalar("Level/Lsadv", losses[3], global_step)
                        writer.add_scalar("Level/Lsadv", losses[4], global_step)

                        writer.add_image("Level/X", x[0][0], global_step)
                        writer.add_image("Level/Y_real", y_real[0][0], global_step)
                        writer.add_image("Level/Y", y[0][0], global_step)
                        writer.add_image("Level/X_AE", losses[5][0], global_step)
                        writer.add_image("Level/X_DeSty", losses[6][0], global_step)
                        writer.add_image("Level/Y_Sy", losses[7][0], global_step)
                    global_step += 1
                print('Iter[%d/%d], Epoch [%d/%d]' %(i+1, outer_iter, epoch+1, epochs))
                print('Lrec: %.3f, Ldadv: %.3f, Ldesty: %.3f, Lsadv: %.3f, Lsty: %.3f'
                         %(losses[0], losses[1], losses[2], losses[3], losses[4]))

    
    print('--- save ---')
    torch.save(tetGAN.state_dict(), opts.save_model_name)

if __name__ == '__main__':
    main()
